<a href='https://codeberg.org/LinuxGamer/realm' target="_blank"><img alt='Codeberg' src='https://img.shields.io/badge/Latest_Commits_Are_Working-100000?style=for-the-badge&logo=Codeberg&logoColor=000000&labelColor=00FF08&color=6F6F6F'/></a>
<!--<a href='https://codeberg.org/LinuxGamer/realm/releases' target="_blank"><img alt='Codeberg' src='https://img.shields.io/badge/Latest_Commits Are NOT Working (Use Release)-100000?style=for-the-badge&logo=Codeberg&logoColor=000000&labelColor=FF0000&color=595959'/></a>-->
# Realm
Realm is a simple productivity suite to help you get work done.

## Features
| Feature | Status | Expected Release |
| ---- | ---- | ---- |
| Homepage dashboard | In Release | alpha-0.1.0 |
| RadioFreeFedi player tab | In Release | alpha-0.1.0
| Timer tab | Not Started | alpha-0.1.1 |
| Pomodoro timer on Timer tab | Not Started | alpha-0.1.1 |

### Homepage
The homepage features a current time clock (based on your systems clock), however the clock is about half a second behind.

### Radio Player
![Radio Player tab in Realm](assets/radio.png)
A simple radio player using vlc and a .pls file to give you access to RadioFreeFedi to chill and relax to whilst working.

    NOTE: Other radio stations (including non open source ones) will probably come in the future.


#### Radios Available
| Radio Station | Available? |
| ---- | ---- |
| Radio Free Fedi (RFF) | Yes |
| Radio Free Fedi Comfy | No (Planned) |


## Releases
[alpha-0.1.0](https://codeberg.org/LinuxGamer/realm/releases/tag/alpha-0.1.0)