import gi
import vlc
from datetime import datetime
import time

gi.require_version("Gtk", "3.0")
gi.require_version('Notify', '0.7')
gi.require_version('GdkX11', '3.0')

from gi.repository import Gtk, Notify, GdkX11
from gi.repository import GObject
  
class MyWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title ="Realm")
        self.set_border_width(0)
        Notify.init("Realm") 
        # Create Notebook
        self.notebook = Gtk.Notebook()
        self.add(self.notebook)

        self.page1 = Gtk.Box(spacing=6)
        self.page1.set_border_width(50)
        self.page1.add(Gtk.Label("Welcome to Realm, a unique productivity suite."))

        self.label = Gtk.Label()
        self.page1.pack_start(self.label, True, True, 0)

        # Schedule a function to update the label every second
        self.timeout_id = GObject.timeout_add_seconds(1, self.update_time)

        self.notebook.append_page(self.page1, Gtk.Label("Home"))


        self.player = None
        self.page2 = Gtk.Box(spacing=6)
        self.page2.set_border_width(10)
        self.page2.add(Gtk.Label("Chill out and relax with RadioFreeFedi!"))

        self.play_btn = Gtk.Button(label="Play")
        #self.play_btn.set_halign(Gtk.Align.CENTER)
        #self.button.set_valign(Gtk.Align.bottom)
        self.play_btn.set_halign(3)
        self.play_btn.set_valign(Gtk.PositionType.BOTTOM)
        self.play_btn.connect("clicked", self.on_play_btn_clicked)
        self.page2.pack_start(self.play_btn, True, True, 0)

        self.playing = False
        
        self.notebook.append_page(self.page2, Gtk.Label("Radios"))

        self.page3 = Gtk.Box()
        self.page3.set_border_width(10)
        self.page3.add(Gtk.Label("Timer to do all your needs"))
        self.notebook.append_page(self.page3, Gtk.Label("Timer"))
  

    def on_play_btn_clicked(self, widget):
        if self.player is None:
            # create VLC instance
            vlc_instance = vlc.Instance("--no-xlib")
            self.player = vlc_instance.media_player_new()

            # create VLC media object from .pls file
            media = vlc_instance.media_new("http://37.58.57.168:8010/rff")

            # set media to player
            self.player.set_media(media)

        if self.player.get_state() == vlc.State.Playing:
            self.player.pause()
            self.play_btn.set_label("Play")
            n = Notify.Notification.new("Realm", "Radio Paused!")
        else:
            self.player.play()
            self.play_btn.set_label("Pause")
            n = Notify.Notification.new("Realm", "Radio Playing!")
        n.show()
  
    def update_time(self):
        # Update the label text with the current time
        now = datetime.now()
        self.label.set_text(now.strftime("%H:%M:%S"))
        # Return True to keep the timeout running
        return True
  
win = MyWindow()
win.connect("destroy", Gtk.main_quit)
# Display the window.
win.show_all()
# Start the GTK + processing loop
Gtk.main()
