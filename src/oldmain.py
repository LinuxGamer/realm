import gi
import vlc

gi.require_version("Gtk", "3.0")
gi.require_version('Notify', '0.7')
gi.require_version('GdkX11', '3.0')

from gi.repository import Gtk, Notify, GdkX11
#from gi.repository import Notify

class MyWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Audiorealm")
        Gtk.Window.set_default_size(self, 640, 480)
        Notify.init("Audiorealm")

        # create VLC instance and media player
        # media = self.instance.media_new('http://37.58.57.168:8010/rff')
#        self.instance = vlc.Instance('http://37.58.57.168:8010/rff')
#        self.player = self.instance.media_player_new()
#        self.player.set_fullscreen(True)
        self.player = None
       
        self.box = Gtk.Box(spacing=6)
        self.add(self.box)

        self.back_btn = Gtk.Button(label="Back")
        self.back_btn.set_halign(2)
        self.back_btn.set_valign(Gtk.PositionType.BOTTOM)
        self.back_btn.connect("clicked", self.on_back_btn_clicked)
        self.box.pack_start(self.back_btn, True, True, 0)

        self.play_btn = Gtk.Button(label="Play")
        #self.play_btn.set_halign(Gtk.Align.CENTER)
        #self.button.set_valign(Gtk.Align.bottom)
        self.play_btn.set_halign(3)
        self.play_btn.set_valign(Gtk.PositionType.BOTTOM)
        self.play_btn.connect("clicked", self.on_play_btn_clicked)
        self.box.pack_start(self.play_btn, True, True, 0)

        self.fore_btn = Gtk.Button(label="Forward")
        #self.back_btn.set_halign(Gtk.Align.CENTER)
        self.fore_btn.set_halign(1)
        self.fore_btn.set_valign(Gtk.PositionType.BOTTOM)
        self.fore_btn.connect("clicked", self.on_fore_btn_clicked)
        self.box.pack_start(self.fore_btn, True, True, 0)

        self.playing = False

#    def on_play_btn_clicked(self, widget):
#        if not self.playing:
#            self.playing = True
#            self.player.play()
#            widget.set_label("Pause")
#            n = Notify.Notification.new("Audiorealm", "Radio Playing!")
#        else:
#            self.playing = False
#            self.player.pause()
#            widget.set_label("Play")
#            n = Notify.Notification.new("Audiorealm", "Radio Paused!")
#        n.show()

    def on_play_btn_clicked(self, widget):
        if self.player is None:
            # create VLC instance
            vlc_instance = vlc.Instance("--no-xlib")
            self.player = vlc_instance.media_player_new()

            # create VLC media object from .pls file
            media = vlc_instance.media_new("http://37.58.57.168:8010/rff")

            # set media to player
            self.player.set_media(media)

        if self.player.get_state() == vlc.State.Playing:
            self.player.pause()
            self.play_btn.set_label("Play")
            n = Notify.Notification.new("Audiorealm", "Radio Paused!")
        else:
            self.player.play()
            self.play_btn.set_label("Pause")
            n = Notify.Notification.new("Audiorealm", "Radio Playing!")
        n.show()

    def on_back_btn_clicked(self, widget):
        n = Notify.Notification.new("Audiorealm", "Radio Back!")
        n.show()

    def on_fore_btn_clicked(self, widget):
        n = Notify.Notification.new("Audiorealm", "Radio Forward!")
        n.show()

win = MyWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
